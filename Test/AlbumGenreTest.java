import java.util.*;

import static org.junit.Assert.*;
import org.junit.*;
import ca.ubc.ece.eece210.mp2.*;

public class AlbumGenreTest {
	
	@Test
	public void testAlbumAndGenre() {		
		ArrayList<String> AAWSongList = new ArrayList<String>(Arrays.asList( "Intro",
																		     "(Interlude 1)",
																		     "Tesselate",
																		     "Breezeblocks",
																		     "(Interlude 2)",
																		     "Something Good",
																		     "Dissolve Me",
																	      	 "Matilda",
																	      	 "MS",
																	      	 "Fitzpleasure",
																	      	 "(Interlude 3)",
																	      	 "Bloodflood",
																	      	 "Taro" ));
		
		ArrayList<String> AMSongList = new ArrayList<String>(Arrays.asList(  "Do I Wanna Know",
																			 "R U Mine?",
																			 "One For The Road",
																			 "Arabella",
																			 "I Want It All",
																			 "No. 1 Party Anthem",
																			 "Mad Sounds",
																			 "Fireside",
																			 "Why'd You Only Call Me When You're High?",
																			 "Snap Out Of It",
																			 "Knee Socks",
																			 "I Wanna Be Yours" ));
		
		ArrayList<String> SoSSongList = new ArrayList<String>(Arrays.asList( "Sultans of Swing",
																			 "Lady Writer",
																			 "Romeo and Juliet",
																			 "Tunnel of Love",
																			 "Private Investigations",
																			 "Money For Nothing",
																			 "Walk Of Life",
																			 "Your Latest Trick",
																			 "Local Hero",
																			 "Skateaway",
																			 "Telegraph Road" ));
																			
		
		// Create a new album and genre and add the album to the genre
		Genre testGenre = new Genre( "Alternative" );
		Album testAlbum = new Album( "An Awesome Wave", "Alt-J", AAWSongList );
		testAlbum.addToGenre(testGenre);
		// Check that testAlbum's genre is testGenre
		assertEquals(testAlbum.getGenre(), testGenre);
		
		// Save the album to a string
		String reconAlbumString = testAlbum.toString();
		System.out.println(reconAlbumString);
		System.out.println(testGenre.toString());
		
		// Reconstruct the album from a string
		Album reconAlbum = new Album(reconAlbumString);
		System.out.println("Test album: " + testAlbum);
		System.out.println("Recon Album: " + reconAlbum);
		assertEquals(testAlbum.toString(), reconAlbum.toString());
		
		// Save the genre to a string
		String reconGenreString = testGenre.toString();
		System.out.println(reconGenreString);
		
		// Reconstruct the genre from a string
		Genre reconGenre = Genre.restoreCollection(reconGenreString);
		System.out.println("Test Genre: " + testGenre);
		System.out.println("Recon Genre: " + reconGenre);
		assertEquals(testGenre.toString(), reconGenre.toString());
		
		// Remove the album from the genre
		reconAlbum.removeFromGenre();
		assertNull(reconAlbum.getGenre());
		
		// Create a catalogue and add our testGenre
		Catalogue testCatalogue = new Catalogue();
		testCatalogue.add(testGenre);		// add method for testing only
		
		// Take a look at what's in our testCatalogue
		System.out.println( "Test catalogue: " + testCatalogue.getContents().toString());
		
		// Save the catalogue to a file
		testCatalogue.saveCatalogueToFile("test_catalogue");
		
		// Reconstruct the catalogue
		Catalogue reconCatalogue = new Catalogue("test_catalogue.txt");
		reconCatalogue.saveCatalogueToFile("recon_catalogue");
		
		System.out.println( "Recon catalogue: " + reconCatalogue.getContents().toString() );
		System.out.println( "Test Catalouge: " + testCatalogue.getContents().toString() );
		
		// Ensure that the catalogue reconstructed from our file is the same as our original catalogue
		assertEquals( reconCatalogue.getContents().toString(), testCatalogue.getContents().toString() );
		
		// ************************************************************** //
		// ****** What Happens when we have more than one genre??? ****** //
		// ************************************************************** //
		
		// Create three albums
		Album anAwesomeWave = new Album( "An Awesome Wave", "Alt-J", AAWSongList);
		Album AM = new Album( "AM", "Arctic Monkeys", AMSongList );
		Album sultansOfSwing = new Album( "Sultans of Swing", "Dire Straits", SoSSongList );
		
		// Create three genres
		Genre alternative = new Genre( "Alternative" );
		Genre rock = new Genre( "Rock" );
		Genre classicRock = new Genre( "Classic Rock" );
		
		// Add the albums to their genres
		anAwesomeWave.addToGenre(alternative);
		AM.addToGenre(rock);
		sultansOfSwing.addToGenre(classicRock);
		
		// Add Classic Rock as a subgenre of Rock
		rock.addToGenre(classicRock);
		
		// Create a new catalogue
		Catalogue complexCatalogue = new Catalogue();
		complexCatalogue.add(alternative);
		complexCatalogue.add(rock);
		
		// Save the catalogue to a file and reconstruct it
		complexCatalogue.saveCatalogueToFile("complex_catalogue");
		Catalogue complexReconCatalogue = new Catalogue( "complex_catalogue.txt");
		complexReconCatalogue.saveCatalogueToFile("complex_recon_catalogue");
		
		// Check that the original catalogue is the same as the 
		assertEquals( complexCatalogue.getContents().toString(), complexReconCatalogue.getContents().toString() );
	}
}
