package ca.ubc.ece.eece210.mp2;

import java.io.*;
import java.util.*;
import ca.ubc.ece.eece210.mp2.Element;

/**
 * Container class for all the albums and genres. Its main 
 * responsibility is to save and restore the collection from a file.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Catalogue {
	private List<Element> contents;
		
	/**
	 * Builds a new, empty catalogue.
	 */
	public Catalogue() {
		this.contents = new ArrayList<Element>();
	}

	/**
	 * Builds a new catalogue and restores its contents from the 
	 * given file.
	 * 
	 * @param fileName
	 *            		the file from where to restore the library.
	 */
	public Catalogue(String fileName) {
		this.contents = new ArrayList<Element>();
		FileInputStream inStream;
		BufferedReader reader;
		String line;
		StringBuilder sb = new StringBuilder();
		
		try {
			inStream = new FileInputStream( fileName );
			reader = new BufferedReader( new InputStreamReader( inStream ) );
			int subgenres = 0;
			
			while( (line = reader.readLine()) != null ) {
				line = line.replace("\t", "");
				// Add the line to sb
				sb.append( line + "\n" );
				// If we encounter a subgenre, increment the subgenres counter
				if( line.equals(Element.createHtmlTag(Element.GENRE, true)) ) {
					subgenres++;
				}
				// If we encounter the end of a subgenre, decrement the subgenres counter
				else if( line.equals(Element.createHtmlTag(Element.GENRE, false)) && subgenres > 1 ) {
					subgenres--;
				}
				// If the line denotes the end of the main genre, create a new genre from sb
				else if( line.equals(Element.createHtmlTag(Element.GENRE, false)) && subgenres == 1 ) {
					sb.append( line + "\n" );
					Genre genre = Genre.restoreCollection(sb.toString());
					this.contents.add(genre);
					// Reset sb to "" after the end of each genre
					sb.delete(0, sb.length());
					subgenres--;
				}
			}
			// Close InputStream and BufferedReader
			inStream.close();
			reader.close();
		} catch (FileNotFoundException fnfe ) {
			System.out.println( "Error: file cannot be read because it cannot be found" );
		} catch ( IOException ioe ) {
			System.out.println( "Error: line cannot be read" );
		}
	}

	/**
	 * Saves the contents of the catalogue to a new file with name fileName.
	 * 
	 * @param fileName 
	 * 					the file where to save the library, should not
	 * 					include a filename extension
	 * 
	 */
	public void saveCatalogueToFile(String fileName) {
		StringBuilder fileContents = new StringBuilder( "" );
		PrintWriter writer;
		try {
			writer = new PrintWriter( fileName + ".txt" );
			for( Element nextElement : contents ) {
				fileContents.append( nextElement.toString() );
			}
			writer.write( fileContents.toString() );
			writer.close();
		} catch (FileNotFoundException e) {
			System.out.println( "Error: file not found" );
		}
	}
	
	/**
	 * Adds an element to the catalogue (for testing purposes)
	 * 
	 * @param Element 
	 * 					the element to be added
	 */
	public void add( Element e ) {
		this.contents.add(e);
	}

	/** 
	 * Getter method for Catalogue's contents
	 * 
	 * @return 
	 * 			this Catalogue's contents in an Arraylist
	 */
	public List<Element> getContents() {
		return this.contents;
	}
}
