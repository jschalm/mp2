package ca.ubc.ece.eece210.mp2;

import java.util.ArrayList;
import java.util.Stack;

/**
 * @author Sathish Gopalakrishnan
 * 
 * This class contains the information needed to represent 
 * an album in our application.
 * 
 */

public final class Album extends Element {
	private String title;
	private String performer;
	private ArrayList<String> songList;
	private Genre genre;
	
	/**
	 * Builds an album with the given title, performer and song list
	 * 
	 * @param title
	 *            the title of the album
	 * @param author
	 *            the performer 
	 * @param songlist
	 * 			  the list of songs in the album
	 */
	public Album(String title, String performer, ArrayList<String> songlist) {
		this.title = title;
		this.performer = performer;
		this.songList = songlist;
	}

	/**
	 * Builds an album from the string representation of the object. It is used
	 * when restoring an album from a file.
	 * 
	 * @requires input string must be formatted in the same way as Album.toString()
	 * 
	 * @param stringRepresentation
	 *            the string representation in HTML
	 */
	public Album(String stringRepresentation) {
		String sr = stringRepresentation.replace("\t", "");
		String[] lines = sr.split("\n");
		String line;
		Stack<HtmlTag> stack = new Stack<HtmlTag>();
		this.songList = new ArrayList<String>();
		
		for( int i = 0; i < lines.length; i++) {
			line = lines[i];
			// If a line starts with "<" then it is an HTML tag line
			if(line.startsWith("<")) {
				// Reduce the line to the element only and create an HtmlTag
				HtmlTag tag = HtmlTag.parseTag(line);
				if(tag.isOpenTag()) {
					stack.push(tag);
				}
				else {
					stack.pop();
				}
			}
			// Otherwise it is a data line
			else {
				if( stack.peek().getElement().equals(TITLE) ) {
					this.title = line;
				}
				else if( stack.peek().getElement().equals(PERFORMER) ) {
					this.performer = line;
				}
				else if( stack.peek().getElement().equals(SONG) ) {
					this.songList.add(line);
				}
			}
		}
	}

	/**
	 * Returns the string representation of the given album in HTML
	 * 
	 * @return the string representation
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder("");
		
		sb.append(createHtmlTag(ALBUM, true) + "\n");
		sb.append("\t" + createHtmlTag(TITLE, true) + "\n\t\t" + title + "\n\t" + createHtmlTag(TITLE, false) + "\n");
		sb.append("\t" + createHtmlTag(PERFORMER, true) + "\n\t\t" + this.performer + "\n\t" + createHtmlTag(PERFORMER, false) + "\n");
		for( String song : this.songList ) {
			sb.append("\t" + createHtmlTag(SONG, true) + "\n\t\t" + song + "\n\t" + createHtmlTag(SONG, false) + "\n");
		}
		sb.append(createHtmlTag(ALBUM, false) + "\n");
		
		return sb.toString();
	}
		
	/**
	 * Adds this album to the given genre.
	 * 
	 * @modifies changes this album's genre field to
	 * the input genre. Adds this album to the input
	 * genre's children.
	 * 
	 * @param genre
	 *            the genre to add the album to.
	 */
	public void addToGenre(Genre genre) {
		if( this.genre != null) {
			this.genre.removeChild(this);
		}
		this.genre = genre;
		genre.addToGenre(this);
	}
	
	/**
	 * Removes this album from its genre, if it belongs to 
	 * a genre.
	 * 
	 */
	public void removeFromGenre() {
		if( this.genre != null )  {
			genre.removeChild(this);
			this.genre = null;
		}
	}

	/**
	 * Returns the genre that this album belongs to.
	 * 
	 * @return the genre that this album belongs to, if 
	 * this album does not belong to a genre, returns null.
	 */
	public Genre getGenre() {
		return this.genre;
	}

	/**
	 * Returns the title of the album
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Returns the performer of the album
	 * 
	 * @return the performer
	 */
	public String getPerformer() {
		return this.performer;
	}

	/**
	 * An album cannot have any children (it cannot contain anything).
	 */
	@Override
	public boolean hasChildren() {
		return false;
	}
}