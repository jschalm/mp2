package ca.ubc.ece.eece210.mp2;

import java.util.*;

/**
 * An abstract class to represent an entity in the catalogue. The element (in
 * this implementation) can either be an album or a genre.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public abstract class Element {
	List<Element> children;
	
	// String conversion encoding //
	static final String GENRE = "genre";
	static final String NAME = "genre name";
	static final String ALBUM = "album";
	static final String TITLE = "title";
	static final String PERFORMER = "performer";
	static final String SONG = "song";


	/**
	 * Returns all the children of this entity. They can be albums or genres. In
	 * this particular application, only genres can have children. Therefore,
	 * this method will return the albums or genres contained in this genre.
	 * 
	 * @return the children
	 */
	public List<Element> getChildren() {
		return children;
	}

	/**
	 * Adds a child to this entity. Basically, it is adding an album or genre to
	 * an existing genre
	 * 
	 * @param b
	 *            the entity to be added.
	 */
	protected void addChild(Element b) throws IllegalArgumentException {
		if( this.hasChildren() ) {
			children.add( b );
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Removes an element from this elements set of children.
	 * 
	 * @param b the element to remove
	 * 
	 * @throws NoSuchElementException if the element does not exist in this element's
	 * set of children
	 * 
	 * @throws IllegalArgumentException if this element has no children
	 */
	protected void removeChild( Element b ) throws NoSuchElementException, IllegalArgumentException {
		if( this.hasChildren() ) {
			if( children.contains(b)) {
				children.remove(b);
			} else {
				throw new NoSuchElementException();
			}
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Creates an encoding string in HTML for an album or genre.
	 * 
	 * @param element
	 * 					string denoting the kind of tag, GENRE or ALBUM
	 * 
	 * @param isOpenTag	
	 * 					true if the tag is an opening tag, false otherwise
	 * 
	 * @return a String representation of the HTML tag
	 */
	protected static String createHtmlTag(String element, boolean isOpenTag) {
		if( isOpenTag ) {
			return "<" + element + ">";
		} 
		else {
			return "</" + element + ">";
		}
	}

	/**
	 * Abstract method to determine if a given entity can (or cannot) contain
	 * any children.
	 * 
	 * @return true if the entity can contain children, or false otherwise.
	 */
	public abstract boolean hasChildren();
	
}