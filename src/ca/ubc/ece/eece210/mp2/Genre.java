package ca.ubc.ece.eece210.mp2;

import java.util.*;

/**
 * Represents a genre (or collection of albums/genres).
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Genre extends Element {
	private String name;
	
	/**
	 * Creates a new genre with the given name.
	 * 
	 * @param name
	 *            the name of the genre.
	 */
	public Genre(String name) {
		this.name = name;
		this.children = new ArrayList<Element>();
	}

	/**
	 * Restores a genre from its given string representation.
	 * String representation should take the form:
	 * 
	 * @requires input string must be properly formatted in the 
	 * same way as Genre.toString()
	 * 
	 * @param stringRepresentation
	 * 								HTML representation of the genre
	 */
	public static Genre restoreCollection(String stringRepresentation) {
		String sr = stringRepresentation.replace("\t", "");
		String[] lines = sr.split( "\n" );
		String line;
		StringBuilder sb = new StringBuilder();
		Genre topLevelGenre = new Genre( lines[2] );
		
		int i = 4;
		while(i < lines.length) {
			line = lines[i];
			
			HtmlTag tag = HtmlTag.parseTag(line);
			// If the line indicates an album, create a new album and add it to topLevelGenre
			if( tag.getElement().equals(ALBUM) ) {
				sb.append(lines[i] + "\n");
				do {
					i++;
					sb.append(lines[i] + "\n");
				} while( !lines[i].equals(Element.createHtmlTag(ALBUM, false)));
				Album album = new Album(sb.toString());
				album.addToGenre(topLevelGenre);	// adds a reference to the album and adds the album as a child in genre
			}
			// If the line indicates the beginning of a subgenre, create the genre and add it to topLevelGenre
			else if( tag.getElement().equals(GENRE) && tag.isOpenTag() ) {
				sb.append(lines[i] + "\n");
				int subgenres = 0;	// keeps track of nested subgenres of the top level genre
				do {
					i++;
					if( lines[i].equals(Element.createHtmlTag(GENRE, true)) ) { 	// check for nested subgenres
						subgenres++;
					}
					if( lines[i].equals(Element.createHtmlTag(GENRE, false)) && subgenres > 0 ) {
						subgenres--;
					}
					sb.append(lines[i] + "\n");
				} while( !lines[i].equals(Element.createHtmlTag(GENRE, false)) && subgenres < 1 );
				Genre subgenre = Genre.restoreCollection(sb.toString());	// create the genre using recursion
				topLevelGenre.addToGenre(subgenre);
			}
			sb.delete(0, sb.length()); 	// reset sb after each iteration through a genre or album
			i++;
		}
		return topLevelGenre;
	}
	
	/**
	 * Returns the string representation of a genre in HTML
	 * 
	 * @return
	 * 			the string representation of this genre in HTML
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder( "" );
		
		sb.append(createHtmlTag(GENRE, true) + "\n");
		sb.append("\t" + createHtmlTag(NAME, true) + "\n\t\t" + this.name + "\n\t" + createHtmlTag(NAME, false) + "\n");
		for( Element e : this.children ) {
			sb.append(e.toString());
		}
		sb.append(createHtmlTag(GENRE,  false) + "\n");
		return sb.toString();
	}

	/**
	 * Adds the given album or genre to this genre
	 * 
	 * @modifies adds the Element to this genre's children
	 * 
	 * @param b
	 *            the element to be added to the collection.
	 */
	public void addToGenre(Element b) {
		addChild(b);
	}

	/**
	 * Returns true, since a genre can contain other albums and/or
	 * genres.
	 */
	@Override
	public boolean hasChildren() {
		return true;
	}
}